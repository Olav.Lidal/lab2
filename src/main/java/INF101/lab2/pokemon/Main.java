package INF101.lab2.pokemon;

public class Main {

    public static Pokemon pokemon1;
    public static Pokemon pokemon2;
    public static void main(String[] args) {
        ///// Oppgave 6
        // Have two pokemon fight until one is defeated
        pokemon1 = new Pokemon("Arcanine", 90, 105);
        pokemon2 = new Pokemon("Snorlax", 160, 85);
        int round = 0;
        while (pokemon1.isAlive() && pokemon2.isAlive()) {
            if (round % 2 == 0) {
                pokemon1.attack(pokemon2);
            }
            if (round % 2 == 1) {
                pokemon2.attack(pokemon1);
            }
            round ++;
        }
    }
}